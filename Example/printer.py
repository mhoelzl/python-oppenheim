print('Loading printer.py')
print(f"__name__ is {__name__}")

if __name__ == '__main__':
    print('printer.py started as main program.')


def print_greeting(name):
    print(f"Hallo, {name}.")